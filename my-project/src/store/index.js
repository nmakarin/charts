import Vue from 'vue'
import Vuex from 'vuex'
const initialData = require('@/data/chart-data.json')


Vue.use(Vuex)

const state = {
  chartData: initialData
}

const getters = {
  chartData: state => state.chartData  // получаем список заметок из состояния
}

const mutations = {
  updateChartData(state, chartData) {
    state.chartData = chartData
  }
}

const actions = {
  updatedChartData({ commit }, data) {
    commit('updateChartData', data)
  }
}

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})